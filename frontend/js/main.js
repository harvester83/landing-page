$(document).ready(function() {
    $('#feedback-form').validate({
        rules: {
            feedback_name: "required",
            feedback_surname: "required",
            feedback_phone: "required",

            feedback_email: {
                required: true,
                email: true
            }
        }
    });

    $.extend(jQuery.validator.messages, {
        required: "Это поле обязательно к заполнению",
        email: "Пожалуйста, введите действительный адрес электронной почты",
        number: "Пожалуйста введите корректно номер телефона",
    });

    $('#connection-container__submit').on('click', function(e) {
        $('#feedback-form').valid();
    });
});